# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=postmarketos-mkinitfs
pkgver=0.16
pkgrel=1
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	busybox-extras
	bzip2
	charging-sdl
	multipath-tools
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	kmod
	lddtree
	lz4
	osk-sdl
	parted
	triggerhappy
	xz
	"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="00-default.modules
	init.sh.in
	init_functions.sh
	mkinitfs.sh
	mkinitfs_functions.sh
	mkinitfs_test.sh
	"
arch="noarch"
license="GPL-2.0-or-later"
provides="mkinitfs=0.0.1"

package() {
	for file in init.sh.in init_functions.sh mkinitfs_functions.sh; do
		install -Dm644 "$srcdir/$file" \
			"$pkgdir/usr/share/postmarketos-mkinitfs/$file"
	done

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 "$srcdir/mkinitfs.sh" \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	/bin/busybox sh ./mkinitfs_test.sh
}

sha512sums="5037cb7285bb7c0c40ca9e6df332d882ef9a8b379756c785f921e062dab1b7e7f3139d00897f69323a916d709ced4297fea8cbd3a13ebae575b873ec9e2cbfae  00-default.modules
7fc6ba96419628850984df3322b73bcf655cba03fe3e97ec7425e66e90f4b2a4de106b5cb2e3d46785a92557334288011334104285e2050c7a8e823b8fa5668c  init.sh.in
2273bbda86954bd1b424ac55cb7d8d04e7fb19f318caad5f5cdfaa996681f04de788be83b02cabda7b6d07dabd44ff71be5c11f3969ba5f7fe4d8f95a2de8b4c  init_functions.sh
dfc01ee0547ea88b7aa45a005e842b636e9e19bbf1705f3dad53a66d57af7c5c513c092b5469a06d9b00322e56a4d25f1b47e4c5324aafa99f5291679968d1f1  mkinitfs.sh
874b1bba86085a4b7707b3d1ac6534e2027b5783111b2f9c66c2595ada95113a95272c1be9d234b7957c785978de0c5352b4abd70819871caed9efcfbf703dac  mkinitfs_functions.sh
c7a3c33daeb12b33ac72207191941c4d634f15c22958273b52af381a70ebaba1d3a9299483f0c447d9e66c560151fe7b9588bb4bbef2c8914f83185984ee4622  mkinitfs_test.sh"
